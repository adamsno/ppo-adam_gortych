#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Point {
	public:
		int x;
		int y;
		
		Point() {
		}
		
		Point(int x, int y) {
			this->x = x;
			this->y = y;
			cout << "Square [" << this->x << ", " << this->y << "] has been created." << endl;
		}
		
		~Point() {
			
		}
		
		void movePoint(int xAxisShift, int yAxisShift) {
			this->x += xAxisShift;
			this->y += yAxisShift;
		}
};

class Square {
	public:
		Point top;
		int length;
		
		Square(Point top, int length) {
			this->top = top;
			this->length = length;
		}
		
		void getCoordinates() {
			cout << "x1: " << this->top.x << " y1: " << this->top.y << endl;
			cout << "x2: " << this->top.x+length  << " y2: " << this->top.y << endl;
			cout << "x3: " << this->top.x  << " y3: " << this->top.y+length << endl;
			cout << "x4: " << this->top.x+length << " y4: " << this->top.y+length << endl;
		}
};

int main() {
	srand(time(NULL));
	
	int inputX = 0, inputY = 0;
	int inputLength = 1;
	
	Point p = Point(inputX, inputY);;
	Square c = Square(p, inputLength);
	
	c.top.movePoint(rand() % 10, rand() % 10);
	c.getCoordinates();
	
	return 0;
}
