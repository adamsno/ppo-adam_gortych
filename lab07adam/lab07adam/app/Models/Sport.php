<?php

namespace Scheduler\Models;

class Sport extends SemesterClass {

	public function getFormName(): string {
		return "wf";
	}

    public function getColor(): string
    {
        return "Cyan";
    }
}
