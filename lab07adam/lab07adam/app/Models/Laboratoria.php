<?php

namespace Scheduler\Models;

class Laboratoria extends SemesterClass {

	public function getFormName(): string {
		return "laboratorium";
	}

	public function getColor(): string
    {
        return "Blue";
    }

}
