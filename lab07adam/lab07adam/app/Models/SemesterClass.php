<?php

namespace Scheduler\Models;

use Scheduler\Interfaces\SemesterClassInterface;

abstract class SemesterClass implements SemesterClassInterface {

	private $name;
	private $surname;
	private $room;

	public function __construct(string $name, string $surname, string $room) {
		$this->name = $name;
		$this->surname = $surname;
		$this->room = $room;

	}

	public function getName(): string {
		return $this->name;
	}

	public function getSurName(): string {
		return $this->surname;
	}

	public function getRoom(): string {
		return $this->room;
	}

}
