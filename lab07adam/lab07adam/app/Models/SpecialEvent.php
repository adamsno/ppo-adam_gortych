<?php
/**
 * Created by PhpStorm.
 * User: Sno
 * Date: 12.12.2017
 * Time: 19:21
 */

namespace Scheduler\Models;


use Scheduler\Interfaces\SemesterClassInterface;

class SpecialEvent implements SemesterClassInterface
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
        // TODO: Implement getName() method.
    }

    public function getFormName(): string
    {
        return "wydarzenia";
        // TODO: Implement getFormName() method.
    }

    public function getColor(): string
    {
        return "Dark-Green";
    }
}