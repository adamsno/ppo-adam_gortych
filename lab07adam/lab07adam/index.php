<?php

require "./vendor/autoload.php";

use Scheduler\Models\Lecture;
use Scheduler\Models\Laboratoria;
use Scheduler\Models\Project;
use Scheduler\Models\Seminar;
use Scheduler\Models\Exercises;
use Scheduler\Models\Schedule;
use Scheduler\Models\Sport;
use Scheduler\Models\Lectureship;

$twig = new Twig_Environment(new Twig_Loader_Filesystem("/"), []);

$schedule = new Schedule();

$schedule->save(new Lecture("Bazy danych", "Klosow", "C212"), 1, 1);
$schedule->save(new Laboratoria("Programowanie i projektowanie obiektowe", "Klosow", "C212"), 1, 2);
$schedule->save(new Project("Angielski", "Ardeli", "C311"), 1, 3);
$schedule->save(new Seminar("Sieci Komputerowe", "Nadybski", "C212"), 1, 4);
$schedule->save(new Lecture("Sieci Komputerowe", "Nadybski", "C11"), 1, 5);
$schedule->save(new Exercises("Podstawy Teorii Informacji", "Grzybowski", "A118"), 2, 3);
$schedule->save(new Lecture("Podstawy Teorii Informacji", "Grzybowski", "A118"), 2, 4);
$schedule->save(new Lecture("Prohabilistyka i statystyka", "Selwat", "C212"), 3, 1);
$schedule->save(new Seminar("Prohabilistyka i statystyka", "Selwat", "C212"), 3, 2);
$schedule->save(new Lectureship("Bazy Danych", "Górska-Zając", "A218"), 3, 3);
$schedule->save(new Laboratoria("Programowanie i projektowanie obiektowe", "Rewak", "C10"), 3, 4);
$schedule->save(new Laboratoria("Programowanie i projektowanie obiektowe", "Rewak", "C10"), 3, 5);
$schedule->save(new Sport("Siłownia", "Wuefista", "A-Wf"), 4, 1);
$schedule->save(new Sport("Siłownia", "Wuefista", "A-WF"), 4, 2);
$schedule->save(new \Scheduler\Models\SpecialEvent("Godziny Rektorskie" ), 5, 1);
$schedule->save(new \Scheduler\Models\SpecialEvent("Immatrykulacja" ), 5, 3);


echo $twig->render("index.twig", [
    "schedule" => $schedule,
]);
