#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	public:
		string studentNo;
		string studentName;
		string studentLast;
		int studentStatus;
		
		void setStudentName(string studentName) {
		this->studentName = studentName;
		}
		string getStudentName() {
		return this->studentName;
		}
		
		void setStudentLast(string studentLast) {
		this->studentLast = studentLast;
		}
		
		string getStudentLast() {
		return this->studentLast;
		}
		
					
		void setStudentNo(string studentNo) {
		this->studentNo = studentNo;
		}
		
		string getStudentNo() {
		return this->studentNo;
		}
		
		void setStudentStatus(int studentStatus) {
		this->studentStatus = studentStatus;
		}
		
		int getStudentStatus() {
		return this->studentStatus;
		}
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}

string getRandomStudentName() {

string list[4] = {"Fifi", "Kondziu", "Adas", "Damianek" };
return list[rand () % 4];
}

string getRandomStudentLast() {
string list[4] = {"Kowal", "Drwal", "Junior", "Senior" };
return list[rand () % 4];
	
}

int getRandomStudentStatus() {
return rand () % 2;
	
}

int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentName(getRandomStudentName());
		student.setStudentLast(getRandomStudentLast());
		student.setStudentStatus(getRandomStudentStatus());
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
				
		{		
		cout << student.getStudentNo()<< " ";
		cout << student.getStudentName()<< " ";
		cout << student.getStudentLast()<< " ";
		cout << student.getStudentStatus()<< endl;
	}
	
		cout  << "Students are active" << endl << endl;
		
		for(int i = 0; i < students.size(); i++) {
			
		if(student.getStudentStatus() == 1 )		
		{		
		cout << student.getStudentNo()<< " ";
		cout << student.getStudentName()<< " ";
		cout << student.getStudentLast()<< endl;
	}
	
	
}
}
return 0;
}


